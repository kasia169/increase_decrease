/**
 * Created by RENT on 2017-08-24.
 */
public class MyClass {

        private int number = 0;

        public MyClass(int number) {
            this.number = number;
        }

        public void increase() {
            ++number;
        }

        public void decrease() {
            --number;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

       @Override
    public String toString(){
            return  String.format("%s", number);
       }
    }


